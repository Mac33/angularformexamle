import { Component, Input, OnInit } from '@angular/core';
import { StockItemsService } from '../core/stock-items.service';
import { StockItem } from '../stockItem';

@Component({
  selector: 'app-stock-items-list',
  templateUrl: './stock-items-list.component.html',
  styleUrls: ['./stock-items-list.component.css']
})
export class StockItemsListComponent implements OnInit {

  constructor(private stockItemsService: StockItemsService) { }

  @Input() stockItems?:StockItem[] = [];

  ngOnInit(): void {
    console.log("StockItemsComponet: " + this.stockItems?.length);
   // this.stockItemsService.getStockItems().subscribe(items => this.stockItems = items) ;
  }

}
