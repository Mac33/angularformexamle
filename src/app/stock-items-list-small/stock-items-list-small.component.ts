import { Component, OnInit } from '@angular/core';
import { StockItemsService } from '../core/stock-items.service';
import { StockItem } from '../stockItem';

@Component({
  selector: 'app-stock-items-list-small',
  templateUrl: './stock-items-list-small.component.html',
  styleUrls: ['./stock-items-list-small.component.css']
})
export class StockItemsListSmallComponent implements OnInit {

  constructor(private stockItemsService: StockItemsService) { }

  stockItems:StockItem[] = [];

  ngOnInit(): void {
    this.stockItemsService.getStockItems().subscribe(items => this.stockItems = items) ;
  }


}
