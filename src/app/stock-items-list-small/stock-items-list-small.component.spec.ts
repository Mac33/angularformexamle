import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockItemsListSmallComponent } from './stock-items-list-small.component';

describe('StockItemsListSmallComponent', () => {
  let component: StockItemsListSmallComponent;
  let fixture: ComponentFixture<StockItemsListSmallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockItemsListSmallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockItemsListSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
