import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentSmallComponent } from './document-small.component';

describe('DocumentSmallComponent', () => {
  let component: DocumentSmallComponent;
  let fixture: ComponentFixture<DocumentSmallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentSmallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
