import { Component, Input, OnInit } from '@angular/core';
import { DocumentModel } from '../models/models';

@Component({
  selector: 'app-document-small',
  templateUrl: './document-small.component.html',
  styleUrls: ['./document-small.component.css']
})
export class DocumentSmallComponent implements OnInit {

  constructor() { }

  @Input() documet?: DocumentModel;

  ngOnInit(): void {
  }

}
