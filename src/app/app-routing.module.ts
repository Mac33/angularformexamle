import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentComponent } from './document/document.component';
import { MainViewComponent } from './main-view/main-view.component';

const routes: Routes = [
 { path: '', component: DocumentComponent },
 { path: 'document/:id', component: DocumentComponent }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
