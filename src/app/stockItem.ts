
export class StockItem {

  constructor(id: number,
    name: string,
    measureUnit: string,
    unitAmount: number,
    unitPrice: number) {

    this.id = id;
    this.name = name;
    this.measureUnit = measureUnit;
    this.unitAmount = unitAmount;
    this.unitPrice = unitPrice;
    this.totalPrice = unitAmount * unitPrice;
  }

  id: number = 0;
  name: string = "";
  measureUnit: string = "";
  unitAmount: number = 0;
  unitPrice: number = 0;
  totalPrice: number = 0;

}
