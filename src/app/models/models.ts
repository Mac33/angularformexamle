import { StockItem } from "../stockItem";

export interface DocumentModel {
  documentNumber: string;
  purchaser: Purchaser;
  items: StockItem[];
  totalSum: number;
}

export interface Purchaser {
  name: string;
  regNr: string;
}

export interface StockItemModel {
  name: string;
  amount: number;
  measureUnit: string;
  unitPrice: number;
  totalPrice: number;
}
