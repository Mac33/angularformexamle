import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockItemSmallComponent } from './stock-item-small.component';

describe('StockItemSmallComponent', () => {
  let component: StockItemSmallComponent;
  let fixture: ComponentFixture<StockItemSmallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockItemSmallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockItemSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
