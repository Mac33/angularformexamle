import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { StockItem } from '../stockItem';

@Component({
  selector: 'app-stock-item-small',
  templateUrl: './stock-item-small.component.html',
  styleUrls: ['./stock-item-small.component.css']
})
export class StockItemSmallComponent implements OnInit {

  @Input() stockItem?: StockItem;

  constructor(){}

  ngOnInit(): void {
  }

}
