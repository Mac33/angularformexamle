import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { DocumentModel, Purchaser } from '../models/models';
import { StockItem } from '../stockItem';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {


  constructor() { }
  userTestStatus: {}[] = [
    { "id": 0, "name": "Available" },
    { "id": 1, "name": "Ready" },
    { "id": 2, "name": "Started" }
];

  documents :DocumentModel[] = [{
    documentNumber: 'FA-013525',
    purchaser: {
      name: 'Dodo',
      regNr: '1234567890'
    },
    items : [new StockItem(10,"Ponozky","Ks",10,2.5),
             new  StockItem(20,"Tricko ","Ks",5,10.5) ],
    totalSum:77.5
  },
  {
    documentNumber: 'FA-113526',
    purchaser: {
      name: 'Jojo',
      regNr: '2022878EE'
    },
    items : [new StockItem(10,"Jablka","Kg",5,10),
             new  StockItem(20,"Banany ","Kg",3,1.5) ],
    totalSum: 54.5
  },
  {
    documentNumber: 'FA-119838',
    purchaser: {
      name: 'Mario',
      regNr: '3682878JJ'
    },
    items : [new StockItem(10,"Skrutka","Kg",7,20),
             new  StockItem(20,"Matica ","Kg",10,1.5),
             new StockItem(10,"Kliniec","Kg",8,10),
            ],
    totalSum: 235
  }];

  getDocuments(): Observable<DocumentModel[]> {
    //Q: Preco const funguje len v medote a nie v triede ?
    const documets = of(this.documents);
    return documets;
  }


   getDocumentById(id: string|null):Observable<DocumentModel> {
    if(id)
    {
      return of(this.documents.find(doc=> doc.documentNumber==id)!);
    }
    return of(this.documents[0]);
   }

   getFirstDocumentId():string
   {
    return this.documents[0].documentNumber;
   }


}

