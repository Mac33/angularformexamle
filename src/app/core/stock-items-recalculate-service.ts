import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StockItem } from '../stockItem';

@Injectable({
  providedIn: 'root'
})
export class StockItemsRecalculateService {

  constructor() { }

  private refreshSummary = new BehaviorSubject(false);

  raiseRefresh = this.refreshSummary.asObservable();

  refres()
  {
    this.refreshSummary.next(true);
  }

  calculate(item?:StockItem)
  {
    if(item!=null)
    {
      item.totalPrice =(item.unitAmount * item.unitPrice);
      this.refres();
    }
  }
}
