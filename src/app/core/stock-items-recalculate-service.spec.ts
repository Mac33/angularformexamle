import { TestBed } from '@angular/core/testing';

import { StockItemsRecalculateService } from './stock-items-recalculate-service';

describe('StockItemsRecalculateServiceService', () => {
  let service: StockItemsRecalculateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockItemsRecalculateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
