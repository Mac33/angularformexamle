import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { StockItem } from '../stockItem';

@Injectable({
  providedIn: 'root'
})
export class StockItemsService {

  constructor() { }

  private items :StockItem[] = [
    new StockItem(1,"Jablko","kg", 100.5, 1.2),
    new StockItem(2,"Hrusky","kg", 120, 1.5),
    new StockItem(3,"Fidorka","ks", 50, 0.57),
    new StockItem(4,"Sirup","l", 60, 2.5),
    new StockItem(5,"Chlieb","ks", 100, 1.33)];

    getStockItems():Observable<StockItem[]> {
          return of(this.items);
  }
}
