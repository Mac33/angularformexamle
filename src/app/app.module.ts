import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StockItemComponent } from './stock-item/stock-item.component';
import { StockItemsListComponent } from './stock-items-list/stock-items-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StockItemSummaryComponent } from './stock-item-summary/stock-item-summary.component';
import { DocumentComponent } from './document/document.component';
import { PurchaserEditorComponent } from './purchaser-editor/purchaser-editor.component';
import { StockItemsListSmallComponent } from './stock-items-list-small/stock-items-list-small.component';
import { StockItemSmallComponent } from './stock-item-small/stock-item-small.component';
import { MainViewComponent } from './main-view/main-view.component';
import { DocumentsListSmallComponent } from './documents-list-small/documents-list-small.component';
import { DocumentSmallComponent } from './document-small/document-small.component';

@NgModule({
  declarations: [
    AppComponent,
    StockItemComponent,
    StockItemsListComponent,
    StockItemSummaryComponent,
    DocumentComponent,
    PurchaserEditorComponent,
    StockItemsListSmallComponent,
    StockItemSmallComponent,
    MainViewComponent,
    DocumentsListSmallComponent,
    DocumentSmallComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
