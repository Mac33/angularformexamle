import { Component, OnInit } from '@angular/core';
import { DocumentService } from '../core/document-service.service';
import { DocumentModel } from '../models/models';

@Component({
  selector: 'app-documents-list-small',
  templateUrl: './documents-list-small.component.html',
  styleUrls: ['./documents-list-small.component.css']
})
export class DocumentsListSmallComponent implements OnInit {

  constructor(private documetService: DocumentService) { }

  documents:DocumentModel[] = [];

  ngOnInit(): void {
    this.documetService.getDocuments().subscribe(documents => this.documents = documents) ;
  }

}
