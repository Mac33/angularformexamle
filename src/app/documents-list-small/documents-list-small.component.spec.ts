import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsListSmallComponent } from './documents-list-small.component';

describe('DocumentsListSmallComponent', () => {
  let component: DocumentsListSmallComponent;
  let fixture: ComponentFixture<DocumentsListSmallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentsListSmallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsListSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
