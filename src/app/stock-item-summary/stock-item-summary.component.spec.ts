import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockItemSummaryComponent } from './stock-item-summary.component';

describe('StockItemSummaryComponent', () => {
  let component: StockItemSummaryComponent;
  let fixture: ComponentFixture<StockItemSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockItemSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockItemSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
