import { Component, Input, OnInit, Output } from '@angular/core';
import { StockItemsRecalculateService } from '../core/stock-items-recalculate-service';
import { StockItem } from '../stockItem';

@Component({
  selector: 'app-stock-item-summary',
  templateUrl: './stock-item-summary.component.html',
  styleUrls: ['./stock-item-summary.component.css']
})
export class StockItemSummaryComponent implements OnInit {

  constructor(private stockItemCalculate: StockItemsRecalculateService) { }

  @Input() stockItems:StockItem[] = [];

  summary:number=0;

  ngOnInit(): void {

    this.recalc();

    this.stockItemCalculate.raiseRefresh.subscribe(value=>
      {
        this.recalc();
      })
  }



  recalc() {
    this.summary = 0;
    for (var item of this.stockItems) {
        this.summary +=item.totalPrice;
    }
    this.summary = Math.round(this.summary * 100) / 100
  }

}


