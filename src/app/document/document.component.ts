import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DocumentService } from '../core/document-service.service';
import { StockItem } from '../stockItem';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {

  documentForm: FormGroup;
  stockItems?:StockItem[];

  constructor(
    fb: FormBuilder,
    private route: ActivatedRoute,
    private documentService: DocumentService
  ) {
    this.documentForm = fb.group({
      documentNumber: '',
      purchaser: {},
      items: [],
      totalSum: 0
    })
  }

  private id:string = '';

  ngOnInit(): void {

    this.route.params.subscribe(
      params => {


        if (this.route.snapshot.paramMap.get('id'))
        {
          this.id = this.route.snapshot.paramMap.get('id')!;
        }
        else
        {
          this.id = this.documentService.getFirstDocumentId();
        }




        console.log("Load: " + this.id);


        this.documentService.getDocumentById(this.id).subscribe(document => {
          this.stockItems = document.items;
          console.log("Items " + this.stockItems );
          this.documentForm.setValue(document);

        })


      }
  );

  }
}
