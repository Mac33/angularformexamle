import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { filter } from 'rxjs';
import { Purchaser } from '../models/models';

const CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => PurchaserEditorComponent),
  multi: true
};

@Component({
  selector: 'app-purchaser-editor',
  templateUrl: './purchaser-editor.component.html',
  styleUrls: ['./purchaser-editor.component.css'],
  providers: [ CONTROL_VALUE_ACCESSOR ]
})
export class PurchaserEditorComponent implements OnInit, ControlValueAccessor {

  purchaserForm: FormGroup;
  writingValue: boolean = false;

  constructor(
    fb: FormBuilder
  ) {
    this.purchaserForm = fb.group({
      regNr: '',
      name: ''
    })
  }

  ngOnInit(): void {
  }

  writeValue(newValue: Purchaser): void {
    console.log('writing firs value');
    this.writingValue = true;
    this.purchaserForm.setValue(newValue);
    this.writingValue = false;
  }

  registerOnChange(onChangeFn: any): void {
    console.log('registering onChange');
    this.purchaserForm.valueChanges.pipe(
      filter(() => !this.writingValue)
    ).subscribe(newValue => onChangeFn(newValue));
  }

  registerOnTouched(fn: any): void {
  }

}
