import { Component, OnChanges, OnInit, SimpleChanges, Input, ChangeDetectionStrategy } from '@angular/core';
import { StockItem } from '../stockItem';
import { FormGroup, FormBuilder } from '@angular/forms';
import { StockItemsRecalculateService } from '../core/stock-items-recalculate-service';

@Component({
  selector: 'app-stock-item',
  templateUrl: './stock-item.component.html',
  styleUrls: ['./stock-item.component.css'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class StockItemComponent implements OnInit {


  profileForm?: FormGroup;


  constructor(private formBuilder: FormBuilder, private stockItemCalculation: StockItemsRecalculateService) { }

  @Input() stockItem?: StockItem;


  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      inputAmount: [this.stockItem?.unitAmount],
      unitPrice: [this.stockItem?.unitPrice],
      totalPrice: [this.stockItem?.totalPrice],
      itemName: [this.stockItem?.name]
    });

    this.profileForm?.controls["itemName"].valueChanges.subscribe((value) => {
      if (this.profileForm?.controls["itemName"].valid) {
        this.stockItem!.name = value;
      }
    });

    this.profileForm?.controls["inputAmount"].valueChanges.subscribe((value) => {
      if (this.profileForm?.controls["inputAmount"].valid) {
        this.stockItem!.unitAmount = value;
        this.onRecalc();
      }
    })

    this.profileForm?.controls["unitPrice"].valueChanges.subscribe((value) => {
      if (this.profileForm?.controls["unitPrice"].valid) {
        this.stockItem!.unitPrice = value;
        this.onRecalc();
      }
    })

    this.profileForm?.controls["totalPrice"].disable();
    this.onRecalc();
  }



  onRecalc() {
    this.stockItemCalculation.calculate(this.stockItem);
    this.profileForm?.controls["totalPrice"].setValue(this.stockItem?.totalPrice);
  }



}
